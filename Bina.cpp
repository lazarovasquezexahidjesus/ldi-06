#include <stdio.h>

int *decToBin(int decimal, int nBits, int binary[]);
void WriteBin(int binary[], int nBits);

int main(int argc, char *argv[]) {

  int binary[8];
  int i;

  for (i=-20; i<20; i++)
    {
      printf("%d\t-->\t", i);
      WriteBin(decToBin(i, 8, binary), 8);
      printf("\n");
    }

  return 0;
}

void WriteBin(int binary[], int nBits)
{
  int i;
  for (i=0; i<nBits; i++)
    printf("%d", binary[i]);
}

/* Convertimos de número decimal a binario, con nBits bits, y almacenamos el resultado en el array binary[] */
/* binary[0] es el bit de mayor peso. */
int *decToBin(int decimal, int nBits, int binary[]) {
	int  i;

	int flag = 0;
	/* Si es un número negativo, lo hacemos positivo y le restamos 1(*), */
	/* además activamos el flag para saber que es negativo */

	/* (*) El complemento a 2 es igual que el complemento a 1, que es cambiar */
	/* 1->0, 0->1, y luego sumando 1 en binario. Aquí restamos 1 en decimal y */
	/* luego pasamos a binario. */
	if (decimal < 0)
	  {
	    flag = 1;
	    decimal = -decimal-1;
	  }


	while (decimal > 0)
	  {
	    binary[--nBits] = flag^(decimal%2);
	    decimal/=2;
	  }

	/* Con esto terminamos de llenar el array, con 0 a la izquierda si es un número positivo */
	/* y con 1s si es un número negativo */
	while (nBits>0)
	  binary[--nBits] = flag;


	return binary;

}
